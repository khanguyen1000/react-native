import React, { useMemo } from 'react';
import { Text, View, StyleSheet, SectionList } from 'react-native';

const SectionListDemo = ({
    params,
}) => {
    const { title, sectionHeader, sectionItem, sectionList } = useMemo(() => styles, []);
    const array = useMemo(() => [
        {
            "title": "Thông tin tài khoản",
            "data": [
                { "id": "1", "name": "Hồ sơ cá nhân" },
                { "id": "2", "name": "Đổi mật khẩu" },
            ]
        },
        {
            "title": "Thông tin liên hệ",
            "data": [
                { "id": "3", "name": "Bạn bè" },
                { "id": "4", "name": "Nhóm" },
                { "id": "5", "name": "Kỷ niệm" },
                { "id": "6", "name": "Tìm kiếm quanh đây" },
                { "id": "7", "name": "Hẹn hò" },
            ],
        },
        {
            "title": "Cài đặt",
            "data": [
                { "id": "8", "name": "Trợ giúp" },
                { "id": "9", "name": "Cài đặt & quyền riêng tư" },
                { "id": "10", "name": "Điều khoản chính sách" },
            ],
        },
    ], []);


    return (
        <View style={{ backgroundColor: '#fff', paddingTop: 80 }} >
            <Text style={[title]}>CÀI ĐẶT ỨNG DỤNG</Text>
            <SectionList
                style={[sectionList]}
                sections={array}
                renderSectionHeader={({ section }) => (<Text style={[sectionHeader]} >
                    {section.title.toUpperCase()}
                </Text>)}
                renderItem={({ item }) =>
                (<Text style={[sectionItem]} >
                    {item.name}
                </Text>)}
            >

            </SectionList>
        </View >
    )
};

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        padding: 1,
    },
    sectionList: {
        borderTopWidth: 1,
        borderTopColor: '#eeeeee',


    },
    sectionItem: {
        paddingTop: 15,
        paddingLeft: 15,
        paddingBottom: 5,
        fontWeight: '300',
        fontSize: 17,
        marginBottom: 10,
    },
    sectionHeader: {
        padding: 15,
        backgroundColor: '#e9eaed',
        fontSize: 17,
    }
})
export default SectionListDemo;
StyleSheet