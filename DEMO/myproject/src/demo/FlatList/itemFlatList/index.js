import React, { useMemo } from 'react';
import { StyleSheet, Text, View, Dimensions, Image, BackHandler } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const widthDevice = Dimensions.get('window').width;
const ItemShoes = (props) => {
    const { url, name, price } = useMemo(() => props.item, []);
    const classes = useMemo(() => styles, []);
    return (
        <View style={classes.cardItem}>
            <Image style={classes.image} source={url} />
            <Text style={classes.name}>
                {name}
            </Text>
            <Text style={classes.price}>{price} $</Text>
        </View>
    )
};


const styles = StyleSheet.create({
    cardItem: {
        width: widthDevice / 2,
        height: 200,
        marginTop: widthDevice * 20 / 100,
        padding: 10,


    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
    },
    price: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#914000',
        textAlign: 'right',

    },
    name: {
        fontSize: 20,
        height: 50,
        marginBottom: 5,
    }
})


export default ItemShoes;
