import React, { useMemo } from 'react';
import { FlatList, Text, View } from 'react-native';
import ItemShoes from '../itemFlatList';
const { shoes1, shoes2, shoes3, shoes4, shoes5, shoes6, shoes7, shoes8, shoes9, shoes10, shoes11 } = {
    shoes1: require('../../../assets/shoes-Images/shoes-1.jpg'),
    shoes2: require('../../../assets/shoes-Images/shoes-2.jpg'),
    shoes3: require('../../../assets/shoes-Images/shoes-3.jpg'),
    shoes4: require('../../../assets/shoes-Images/shoes-4.png'),
    shoes5: require('../../../assets/shoes-Images/shoes-5.jpg'),
    shoes6: require('../../../assets/shoes-Images/shoes-6.jpg'),
    shoes7: require('../../../assets/shoes-Images/shoes-7.jpg'),
    shoes8: require('../../../assets/shoes-Images/shoes-8.jpg'),
    shoes9: require('../../../assets/shoes-Images/shoes-9.jpg'),
    shoes10: require('../../../assets/shoes-Images/shoes-10.jpg'),
    shoes11: require('../../../assets/shoes-Images/shoes-11.jpg'),
}


const FlatListDemo = () => {
    let arrShoes = useMemo(() => [
        { id: 1, name: `Festive Aurora Black`, price: 55, desc: 'desc', url: shoes1, hot: false },
        { id: 2, name: `Festive Snowflake Blue`, price: 39, desc: 'desc', url: shoes2, hot: false },
        { id: 3, name: `Festive Breezer Black`, price: 41, desc: 'desc', url: shoes3, hot: false },
        { id: 4, name: `Festive Frosty-White D`, price: 50, desc: 'desc', url: shoes4, hot: false },
        { id: 5, name: `Festive Spice Pumpkin`, price: 62, desc: 'desc', url: shoes5, hot: false },
        { id: 6, name: `Nameless Edition`, price: 45, desc: 'desc', url: shoes6, hot: false },
        { id: 7, name: `Festive Washed-Green Grey`, price: 40, desc: 'desc', url: shoes7, hot: false },
        { id: 8, name: `Festive Spice Pumpkin`, price: 66, desc: 'desc', url: shoes8, hot: false },
        { id: 9, name: `Street Festive Low-Cut`, price: 70, desc: 'desc', url: shoes9, hot: false },
        { id: 10, name: `X Matcha`, price: 55, desc: 'desc', url: shoes10, hot: false },
        { id: 11, name: `X Orange Tonic`, price: 64, desc: 'desc', url: shoes11, hot: false },
    ], []);


    return (
        <>
            <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center', padding: 10, letterSpacing: 5 }}>CyberShoes</Text>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

                <FlatList data={arrShoes}
                    renderItem={({ item }) => (<ItemShoes item={item} />)}
                    numColumns={2}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        </>

    )
};

export default FlatListDemo;
