import React, { useCallback, useMemo } from 'react';
import { Dimensions, Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
const widthDevice = Dimensions.get('window').width;
const heightDevice = Dimensions.get('window').height;
const RenderLoop = () => {
    const data = useMemo(() => [
        { ma: 1, tenMon: 'Gà nướng', gia: 10, hinhAnh: require('../../assets/food-Images/ga-nuong.jpg') },
        { ma: 2, tenMon: 'Bò nướng', gia: 20, hinhAnh: require('../../assets/food-Images/bo-nuong.jpg') },
        { ma: 3, tenMon: 'Mì ý', gia: 30, hinhAnh: require('../../assets/food-Images/mi-y.jpg') },
        { ma: 4, tenMon: 'Heo hun khói', gia: 40, hinhAnh: require('../../assets/food-Images/heo-hun-khoi.jpg') },
        { ma: 5, tenMon: 'Cà ri', gia: 50, hinhAnh: require('../../assets/food-Images/ga-ri.jpg') },
        { ma: 6, tenMon: 'Sườn nướng', gia: 60, hinhAnh: require('../../assets/food-Images/suon-nuong.jpg') },
    ], []);
    const renderListData = useCallback(() => {
        return data.map((item, index) => (
            <Col key={index} style={{ width: widthDevice / 2, paddingLeft: 30, paddingTop: 30 }}>
                <Text style={{ fontSize: 20, paddingBottom: 15 }}>{item.tenMon}</Text>
                <Image style={{ width: 150, height: 100, borderRadius: 10 }} source={item.hinhAnh} />
                <Text style={{ fontSize: 20, paddingTop: 15, fontWeight: 'bold', color: '#ff5900' }}>{item.gia.toLocaleString()} $</Text>
                <TouchableOpacity>
                    <View style={{ borderRadius: 2, padding: 5, marginTop: 5, backgroundColor: '#F15F66', width: 150, alignItems: 'center', borderRadius: 5 }}>
                        <Text style={{ color: 'white', fontSize: 20 }}>Đặt món</Text>
                    </View>
                </TouchableOpacity>

            </Col>
        ))
    }, [data]);

    return (
        <ScrollView>
            <Grid style={{ flex: 1 }}>
                <Row style={{ justifyContent: 'space-between', height: 250 }}>

                    <Image style={{ height: 250, width: '100%' }} source={require('../../assets/food-Images/menu.png')} resizeMode="contain" />

                </Row>
                <Row>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 30 }}>CyberFood</Text>
                    </View>
                </Row>
                <Row style={{ flex: 1, flexWrap: 'wrap', justifyContent: 'flex-start', }}>
                    {renderListData()}
                </Row>
            </Grid>
        </ScrollView>
    )
};

export default RenderLoop;
