import React, { useCallback, useMemo, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

const RenderContent = ({
    params,
}) => {
    const [isLogin, setIsLogin] = useState(false);
    let name = useMemo(() => {
        return 'kha';
    }, []);
    const renderContent = useCallback(() => {
        return isLogin ? (<View>
            <Text>Hello {name}</Text>
            <View style={{ padding: 10, backgroundColor: '#F79105', borderRadius: 5 }}>
                <TouchableOpacity onPress={() => setIsLogin(false)}><Text>SignOut</Text></TouchableOpacity>
            </View>


        </View>
        ) : (<TouchableOpacity onPress={() => setIsLogin(true)}>
            <View style={{ padding: 10, backgroundColor: '#ff6c51', borderRadius: 5 }}>
                <Text style={{ fontSize: 20, color: '#fff' }}>Login</Text>
            </View>
        </TouchableOpacity>)
    })
    return (
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>

            {renderContent()}
        </View>
    )
};

export default RenderContent;
