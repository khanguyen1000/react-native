import React from 'react';
import { View } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';

const EasyGrid = (props) => {
    return (
        <Grid>
            <Row style={{ backgroundColor: 'pink', padding: 20 }}>
                <View style={{ backgroundColor: 'white', height: '100%', width: '100%' }}></View>
            </Row>
            <Row style={{ backgroundColor: '#F4B062' }}>

            </Row>
            <Row style={{ backgroundColor: '#8ACFA6' }}>
                <Col style={{ padding: 10 }}>
                    <View style={{ backgroundColor: 'white', height: '100%', width: '100%' }}></View>
                </Col>
                <Col style={{ padding: 10 }}>
                    <View style={{ backgroundColor: 'white', height: '100%', width: '100%' }}></View>
                </Col>
                <Col style={{ padding: 10 }}>
                    <View style={{ backgroundColor: 'white', height: '100%', width: '100%' }}></View>
                </Col>
            </Row>
            <Row >
                <Col style={{ backgroundColor: 'blue' }}></Col>
                <Col style={{ backgroundColor: 'yellow' }}></Col>
            </Row>
            <Row>
                <Col style={{ backgroundColor: '#cccccc' }}></Col>
                <Col style={{ backgroundColor: '#EDE7A2' }}></Col>
            </Row>
            <Row >
                <Col style={{ backgroundColor: '#8ACFA6' }}></Col>
                <Col style={{ backgroundColor: '#D4EDE2' }}></Col>
            </Row>
        </Grid>
    );
}

export default EasyGrid;